import rk_public as cal
import ice_ocean_public as j
import numpy as np
from matplotlib import pylab as plt
##fjord
Ta_start=1



##line plume tidewater glasier
q = 0.1 #subglacial disscharge per glacier width for line plume
sina_scal= 1 #slope of glacier,1=tidewater
Zx0 = -1000 #depth of glacier
values =5000 #regulates stepsize
X= np.linspace(0.0,(abs(Zx0)/sina_scal),values)
sina=0*X+sina_scal 
E0=0.1 #entrainment coefficient


# +++++++++ calculate some variables +++++++++      

Z=(Zx0+X*sina)  # depth of glacier
Ta=X*0+Ta_start #fjord temperature					
Sa=X*0+34.65    #jord salinity
#plume:
T0=0.
S0=1e-06
y0=np.zeros(4)
U0=j.Ujenkins(Sa[0],S0,Ta[0],T0,sina[0],E0,q) #balance velocity


D=q/U0
y0[0]=q
y0[1]=y0[0]*U0
y0[2]=y0[0]*T0
y0[3]=y0[0]*S0


melt_1d,y_1d,Sb_1d=cal.rk(X,Z,Ta,Sa,E0,sina,y0,'line')
plt.plot(melt_1d *3600*24,Z, label='line')

####cone
Q=500 #[m^3/s] total discharge Q=(Pi/2)*D^2 U
S0=1e-06
T0=j.t_freeze(S0,Zx0)

y0=np.zeros(4)
U0=j.Ujenkins_cone(Sa[0],S0,Ta[0],T0,sina[0],E0,Q) #balance velocity


y0[0]=Q*2/np.pi
y0[1]=y0[0]*U0
y0[2]=y0[0]*T0
y0[3]=y0[0]*S0

melt_cone,y_1d,Sb_1d=cal.rk(X,Z,Ta,Sa,E0,sina,y0,'cone')
plt.plot(melt_cone *3600*24,Z, label='cone')
plt.xlabel('melt [m/d]')
plt.ylabel('Z [m]')

plt.legend()
plt.show()

